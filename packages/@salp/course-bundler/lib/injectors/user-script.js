module.exports = options => {
    let code
    let userScript = options.userScript

    if (userScript) {
        // double escape backward slashes for windows
        userScript = userScript.replace(/\\/g, '\\\\')
        code = `export { default } from '${userScript}'`
    } else {
        code = '/* no user script provided */'
    }

    return { code }
}
